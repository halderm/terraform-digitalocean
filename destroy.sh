#!/bin/bash

. ./init.sh

terraform init

terraform refresh \
  -var "do_token=${DO_TOKEN}" \
  -var "pub_key=${SSH_KEY}.pub" \
  -var "pvt_key=${SSH_KEY}" \
  -var "ssh_fingerprint=${SSH_FINGERPRINT}"

terraform plan -destroy -out=DESTROYallTerraforms.tfplan \
  -var "do_token=${DO_TOKEN}" \
  -var "pub_key=${SSH_KEY}.pub" \
  -var "pvt_key=${SSH_KEY}" \
  -var "ssh_fingerprint=${SSH_FINGERPRINT}"

terraform apply DESTROYallTerraforms.tfplan
