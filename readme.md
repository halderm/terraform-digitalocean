# references

[youtube minitutorial](https://www.youtube.com/playlist?list=PLtK75qxsQaMIHQOaDd0Zl_jOuu1m3vcWO)

[digitalocean droplet metadata](https://www.digitalocean.com/community/tutorials/an-introduction-to-droplet-metadata)

# good to know

enable logging with TF\_LOG=1

look into terraform.tfstate which keeps all current state informations

get available regions for digitalocean

```sh
./regions.sh | python -m json.tool
```

# how to use this demo

* install terraform

* create a new digitalocean api key

create token with write access [here](https://cloud.digitalocean.com/settings/api/tokens) and add it in init.sh as DO\_TOKEN

* create a new ssh keypair

```sh
ssh-keygen -f ${HOME}/.ssh/tfdo -N ''
```

* add the key in [digitalocean](https://cloud.digitalocean.com/settings/security) and copy the fingerprint of your ssh key

* add the copied fingerprint as SSH\_FINGERPRINT in init.sh

* create the cluster

```sh
./run.sh
```

* test the loadbalancer

look up the ip of haproxy droplet in [digitalocean](https://cloud.digitalocean.com/dashboard) and open a browser with the ip address of haproxy

* destroy the cluster

```sh
./destroy.sh
```
