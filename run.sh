#!/bin/bash

. ./init.sh

terraform init

terraform refresh \
  -var "do_token=${DO_TOKEN}" \
  -var "pub_key=${SSH_KEY}.pub" \
  -var "pvt_key=${SSH_KEY}" \
  -var "ssh_fingerprint=${SSH_FINGERPRINT}"

terraform plan \
  -var "do_token=${DO_TOKEN}" \
  -var "pub_key=${SSH_KEY}.pub" \
  -var "pvt_key=${SSH_KEY}" \
  -var "ssh_fingerprint=${SSH_FINGERPRINT}"

terraform apply \
  -var "do_token=${DO_TOKEN}" \
  -var "pub_key=${SSH_KEY}.pub" \
  -var "pvt_key=${SSH_KEY}" \
  -var "ssh_fingerprint=${SSH_FINGERPRINT}"
